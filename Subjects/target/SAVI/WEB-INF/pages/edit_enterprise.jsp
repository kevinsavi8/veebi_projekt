<%--
  Created by IntelliJ IDEA.
  User: savi
  Date: 6/8/15
  Time: 12:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app>
<head>
  <meta charset="utf-8">
  <title>Edit enterprise</title>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
  <script>
    function EnterpriseEditController($scope, $http, $location) {
      var enterpriseId = +$location.absUrl().substring($location.absUrl().lastIndexOf('/')+1);
      console.log(enterpriseId);
      if (typeof enterpriseId === 'number') {
        $http.get('/SAVI/enterprises/'+enterpriseId).success(function(data) {
          if (!data) {
            $scope.errorMessage = "Could not find enterprise with that id";
          } else {
            $scope.enterprise = data;
            $http.get('/SAVI/addresses/subject/', {
              params : {
                'subjectId':$scope.enterprise.enterpriseId,
                'subjectTypeId':2
              }
            }).success(function(data) {
              $scope.enterpriseAddresses = data;
              /*$scope.enterpriseAddresses.forEach(function(address) {
                if (address.addressTypeId == 2) {
                  $scope.enterpriseAddress = address;
                }
              });*/
            });
          }
        }).error(function (error) {
          $scope.errorMessage = "Could not find enterprise with that id";
          console.log("Could not get enterprise: ", error);
        });
      }

      $scope.addAnotherAddress = function() {
        $scope.enterpriseAddresses.push({
          'addressTypeId' : 3,
          'subject' : enterpriseId,
          'subjectTypeId' : 2
        });
      };

      $scope.submitEnterprise = function() {
        $scope.enterpriseAddresses.forEach(function(address) {
          if(address.addressId) {
            $http.put('/SAVI/addresses',address).success(function(data) {
            });
          } else {
            $http.post('/SAVI/addresses',address).success(function(data) {
              address.addressId = data;
            });
          }
        });
      }

    }
  </script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
  <div ng-controller="EnterpriseEditController">
  <h1 class="text-center">Edit enterprise</h1>
  <div class="container">
    <ul class="nav nav-tabs">
      <li><a href="../">Home</a></li>
      <li><a href="../search">Search</a></li>
      <li role="presentation" class="dropdown active">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
          Edit <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="../edit_enterprise/1">Enterprise</a></li>
          <li><a href="../edit_person/1">Person</a></li>
        </ul>
      </li>
      <li><a href="../logout">Logout</a></li>
    </ul>
	</div>
    <div class="container col-sm-4 col-sm-offset-4">
      <form name="enterpriseForm" role="form" ng-submit="submitEnterprise()" ng-show="enterprise.enterpriseId">
      <!-- 
      	{{enterprise}}
        {{enterpriseAddresses}}
       -->
        <div class="form-group">
          <label for="name">Name:</label>
          <input class="form-control" id="name" type="text" ng-model="enterprise.name" />
        </div>
        <div class="form-group">
          <label for="full_name">Full name:</label>
          <input class="form-control" id="full_name" type="text" ng-model="enterprise.fullName" />
        </div>
        <div ng-repeat="enterpriseAddress in enterpriseAddresses">
          <label>Address {{$index + 1}}</label>
          <div class="form-group">
            <label for="e_country">Country:</label>
            <input class="form-control" id="e_country"  type="text" ng-model="enterpriseAddress.country" />
          </div>
          <div class="form-group">
            <label for="e_county">County:</label>
            <input class="form-control" id="e_county"  type="text" ng-model="enterpriseAddress.county" />
          </div>
          <div class="form-group">
            <label for="e_townVillage">Town/Village:</label>
            <input class="form-control" id="e_townVillage"  type="text" ng-model="enterpriseAddress.townVillage" />
          </div>
          <div class="form-group">
            <label for="e_address">Address:</label>
            <input class="form-control" id="e_address"  type="text" ng-model="enterpriseAddress.streetAddress" />
          </div>
          <div class="form-group">
            <label for="e_zipCode">Postal code:</label>
            <input class="form-control" id="e_zipCode"  type="text" ng-model="enterpriseAddress.zipCode" />
          </div>
        </div>
        <button class="btn btn-info btn-block" ng-click="addAnotherAddress()">Add another address</button>
        <input class="btn btn-primary btn-block" type="submit" id="e_submit" value="Submit" />

      </form>
    </div>
  </div>
</body>
</html>
