<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
  <title>Log in</title>


  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.3/angular.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

</head>
<body class="preview" id="top" data-spy="scroll">
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 jumbotron" style="margin-top: 50px;">

    	<h2 class="text-center">Log in to system</h2>
		
     
		<c:if test="${not empty param.error}">
		<p style="color: red;">Login failed</p>
		</c:if>
     
		<form action="<%=request.getContextPath()%>/j_spring_security_check" method="post" class="form-horizontal">
			<div class="form-group">
				<label for="inputUser" class="col-sm-2 control-label">User:</label>
				<div class="col-sm-10">
					<input type="text" name="j_username" class="form-control" id="inputUser" placeholder="User">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-10">
					<input type="password" name="j_password" class="form-control" id="inputPassword" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-success btn-block">Log in</button>
				</div>
			</div>
		</form>
	  
    </div>
  </div>
</div>
</body>
</html>