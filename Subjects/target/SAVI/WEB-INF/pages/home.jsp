<%--
  Created by IntelliJ IDEA.
  User: savi
  Date: 6/7/15
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="subjects">
<head>
  <meta charset="utf-8">
  <title>Subjects</title>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
  <script>
    var app = angular.module('subjects', []);

    app.controller('SubjectController', ['$scope', '$http', function($scope, $http) {
      $scope.add = 'person';
      $scope.birthDate = [];

      $scope.clear = function() {
        $scope.person = {};
        $scope.personAddress = {};
        $scope.enterprise = {};
        $scope.enterpriseAddress = {};
        $scope.employee = {};
      };

      $scope.submitPerson = function() {
        var unixtime = Math.round(new Date($scope.person.birthDate).getTime()/1000);
        $scope.person.birthDate = unixtime;
        $http.post('/t120920Subjects/persons/', $scope.person).success(function(data) {
          console.log("Person: ",data);
          $scope.person.personId = data;
          $scope.personAddress.subject = data;
          $scope.personAddress.subjectTypeId = 1;
          $scope.personAddress.addressTypeId = 1;
          $http.post('/t120920Subjects/addresses/', $scope.personAddress).success(function(data) {
            console.log("Person address: ",data);
          });
        });
      };

      $scope.submitEnterprise = function() {
        $http.post('/t120920Subjects/enterprises/', $scope.enterprise).success(function(data) {
          console.log("Enterprise: ",data);
          $scope.enterprise.enterpriseId = data;
          $scope.enterpriseAddress.subject = data;
          $scope.enterpriseAddress.subjectTypeId = 2;
          $scope.enterpriseAddress.addressTypeId = 1;
          $http.post('/t120920Subjects/addresses/', $scope.enterpriseAddress).success(function(data) {
            console.log("Enterprise address: ",data);
          });
        });
      };
    }]);
  </script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

</head>
<body>
  <h1 class="text-center">Subjects application</h1>
  <div ng-controller="SubjectController">
  	<div class="container">
      <ul class="nav nav-tabs">
        <li class="active"><a href="./">Home</a></li>
        <li><a href="search">Search</a></li>
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Edit <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="edit_enterprise/1">Enterprise</a></li>
            <li><a href="edit_person/1">Person</a></li>
          </ul>
        </li>
        <li><a href="logout">Logout</a></li>
      </ul>
	</div>
	<br/>
  	<div class="container text-center">
		<label for="rndinput">Add new: </label>
	    <input id="rndinput" type="radio" ng-model="add" value="person" ng-change="clear()" />Person
	    <input id="rndinput" type="radio" ng-model="add" value="enterprise" ng-change="clear()" />Enterprise

    	<!-- {{add}} -->
  	</div>
    <div class="container col-sm-offset-4 col-sm-4">
      <form name="personForm" role="form" ng-submit="submitPerson()" ng-show="add == 'person'">
        <!-- 
        {{person}}
        {{personAddress}}
         -->
        <div class="form-group">
          <label for="first_name">First name:</label>
          <input class="form-control" id="first_name" type="text" ng-model="person.firstName" />
        </div>
        <div class="form-group">
          <label for="last_name">Last name:</label>
          <input class="form-control" id="last_name" type="text" ng-model="person.lastName" />
        </div>
        <div class="form-group">
          <label for="identity_code">Identity code:</label>
          <input class="form-control" id="identity_code" type="text" ng-model="person.identityCode" />
        </div>
        <div class="form-group">
          <label for="birth_date">Birth date:</label>
          <input class="form-control" id="birth_date" placeholder="YYYY/MM/DD" type="text" ng-model="person.birthDate" />
        </div>
        <div class="form-group">
          <label for="country">Country:</label>
          <input class="form-control" id="country"  type="text" ng-model="personAddress.country" />
        </div>
        <div class="form-group">
          <label for="county">County:</label>
          <input class="form-control" id="county"  type="text" ng-model="personAddress.county" />
        </div>
        <div class="form-group">
          <label for="townVillage">Town/Village:</label>
          <input class="form-control" id="townVillage"  type="text" ng-model="personAddress.townVillage" />
        </div>
        <div class="form-group">
          <label for="address">Address:</label>
          <input class="form-control" id="address"  type="text" ng-model="personAddress.streetAddress" />
        </div>
        <div class="form-group">
          <label for="zipCode">Postal code:</label>
          <input class="form-control" id="zipCode"  type="text" ng-model="personAddress.zipCode" />
        </div>
        <input class="btn btn-primary btn-block" type="submit" id="submit" value="Add new person" />
      </form>
      <a class="btn btn-info btn-block" ng-show="person.personId" href="edit_person/{{person.personId}}" ng-click="">Edit added person</a>
    </div>

    <div class="container col-sm-offset-4 col-sm-4">
      <form name="enterpriseForm" role="form" ng-submit="submitEnterprise()" ng-show="add == 'enterprise'">
        <!--
        {{enterprise}}
        {{enterpriseAddress}} -->
        <div class="form-group">
          <label for="name">Enterprise name:</label>
          <input class="form-control" id="name" type="text" ng-model="enterprise.name" />
        </div>
        <div class="form-group">
          <label for="full_name">Enterprise full name:</label>
          <input class="form-control" id="full_name" type="text" ng-model="enterprise.fullName" />
        </div>
        <div class="form-group">
          <label for="e_country">Country:</label>
          <input class="form-control" id="e_country"  type="text" ng-model="enterpriseAddress.country" />
        </div>
        <div class="form-group">
          <label for="e_county">County:</label>
          <input class="form-control" id="e_county"  type="text" ng-model="enterpriseAddress.county" />
        </div>
        <div class="form-group">
          <label for="e_townVillage">Town/Village:</label>
          <input class="form-control" id="e_townVillage"  type="text" ng-model="enterpriseAddress.townVillage" />
        </div>
        <div class="form-group">
          <label for="e_address">Address:</label>
          <input class="form-control" id="e_address"  type="text" ng-model="enterpriseAddress.streetAddress" />
        </div>
        <div class="form-group">
          <label for="e_zipCode">Postal code:</label>
          <input class="form-control" id="e_zipCode"  type="text" ng-model="enterpriseAddress.zipCode" />
        </div>
        <input class="btn btn-primary btn-block" type="submit" id="e_submit" value="Add new enterprise" />

      </form>
      <a class="btn btn-info btn-block" ng-show="enterprise.enterpriseId" href="edit_enterprise/{{enterprise.enterpriseId}}" ng-click="">Edit added enterprise</a>
    </div>

    <form name="employeeForm">

    </form>
  </div>


</body>
</html>
