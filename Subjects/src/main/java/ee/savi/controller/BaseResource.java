package ee.savi.controller;

import ee.savi.util.JsonUtil;
import ee.savi.util.SpringUtil;

/**
 * Created by savi on 6/7/15.
 */
public class BaseResource {

    protected <T> T getBean(Class<T> clazz) {
        return SpringUtil.getBeanFactory().getBean(clazz);
    }

    protected <T> T getBean(Class<T> clazz, String name) {
        return SpringUtil.getBeanFactory().getBean(name, clazz);
    }

    protected String asJson(Object obj) {
        return JsonUtil.asJson(obj);
    }

    protected <T> T fromJson(String json, Class<T> clazz) {
        return JsonUtil.fromJson(json, clazz);
    }

    protected String asISO8601Json(Object obj) {
        return JsonUtil.asISO8601Json(obj);
    }

    protected <T> T fromISO8601Json(String json, Class<T> clazz) {
        return JsonUtil.fromISO8601Json(json, clazz);
    }
}
