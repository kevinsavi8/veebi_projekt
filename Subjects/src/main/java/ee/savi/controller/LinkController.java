package ee.savi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by savi on 6/7/15.
 */
@Controller
public class LinkController {

    @RequestMapping(value = "/login")
    public ModelAndView loginPage() {
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/")
    public ModelAndView mainPage() {
        return new ModelAndView("home");
    }

    @RequestMapping(value = "/index")
    public ModelAndView indexPage() {
        return new ModelAndView("home");
    }

    @RequestMapping(value = "/search")
    public ModelAndView searchPage() {
        return new ModelAndView("search");
    }

    @RequestMapping(value = "/edit_person/{id}")
    public ModelAndView editPersonPage(@PathVariable Long id) {
        ModelAndView editModel = new ModelAndView("edit_person");
        editModel.addObject(id);
        return editModel;
    }

    @RequestMapping(value = "/edit_enterprise/{id}")
    public ModelAndView editEnterprisePage(@PathVariable Long id) {
        ModelAndView editModel = new ModelAndView("edit_enterprise");
        editModel.addObject(id);
        return editModel;
    }

    @RequestMapping(value = "/add/person")
    public ModelAndView addPersonPage() {
        return new ModelAndView("add-person");
    }
}
