package ee.savi.controller;

import ee.savi.model.SubjectAttribute;
import ee.savi.model.SubjectAttributeListItem;
import ee.savi.service.SubjectAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by savi on 6/7/15.
 */
@RestController
@RequestMapping("/subject-attributes")
public class SubjectAttributeController {

    @Autowired
    private SubjectAttributeService subjectAttributeService;

    @RequestMapping(method = RequestMethod.GET)
    public List<SubjectAttribute> getAll() {
        return subjectAttributeService.getAll();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public SubjectAttribute get(@PathVariable Long id) {
        return subjectAttributeService.get(id);
    }

    @RequestMapping(value = "/person/{id}",method = RequestMethod.GET)
    public List<SubjectAttributeListItem> getPersonAttributes(@PathVariable Long id) {
        return subjectAttributeService.getPersonAttributes(id);
    }

    @RequestMapping(value = "/enterprise/{id}",method = RequestMethod.GET)
    public List<SubjectAttributeListItem> getEnterpriseAttributes(@PathVariable Long id) {
        return subjectAttributeService.getEnterpriseAttributes(id);
    }

    @RequestMapping(value = "/employee/{id}",method = RequestMethod.GET)
    public List<SubjectAttributeListItem> getEmployeeAttributes(@PathVariable Long id) {
        return subjectAttributeService.getEmployeeAttributes(id);
    }

    @RequestMapping(value = "/customer/{id}",method = RequestMethod.GET)
    public List<SubjectAttributeListItem> getCustomerAttributes(@PathVariable Long id) {
        return subjectAttributeService.getCustomerAttributes(id);
    }

    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long insert(@RequestBody SubjectAttribute subjectAttribute) {
        return subjectAttributeService.insert(subjectAttribute);
    }
}
