package ee.savi.controller;

import ee.savi.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by savi on 6/6/15.
 */
@Controller
@RequestMapping("/users")
public class UserController extends CrudRestController<User, Long> {
}
