package ee.savi.controller;

import ee.savi.model.Address;
import ee.savi.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by savi on 6/7/15.
 */
@RestController
@RequestMapping("/addresses")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Address> getAll() {
        return addressService.getAll();
    }

    @RequestMapping(value="/subject", method = RequestMethod.GET)
    public List<Address> getBySubject(@RequestParam("subjectId") Long subjectId, @RequestParam("subjectTypeId") Long subjectTypeId) {
        return addressService.getBySubject(subjectId, subjectTypeId);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public Address get(@PathVariable Long id) {
        return addressService.get(id);
    }

    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long insert(@RequestBody Address address) {
        return addressService.insert(address);
    }

    @RequestMapping(method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody Address address) {
        addressService.update(address);
    }
}
