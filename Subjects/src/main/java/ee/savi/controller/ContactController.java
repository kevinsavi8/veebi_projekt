package ee.savi.controller;

import ee.savi.model.Contact;
import ee.savi.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@RestController
@RequestMapping("/contacts")
public class ContactController extends BaseResource {

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Contact> getAll() {
        return contactService.getAll();
    }
}
