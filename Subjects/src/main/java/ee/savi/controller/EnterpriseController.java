package ee.savi.controller;

import ee.savi.model.Enterprise;
import ee.savi.service.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by savi on 6/7/15.
 */
@RestController
@RequestMapping("/enterprises")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Enterprise> getAll() {
        return enterpriseService.getAll();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public Enterprise get(@PathVariable Long id) {
        return enterpriseService.get(id);
    }

    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long insert(@RequestBody Enterprise enterprise) {
        return enterpriseService.insert(enterprise);
    }
}
