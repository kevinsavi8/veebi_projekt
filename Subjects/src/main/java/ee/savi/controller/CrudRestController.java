package ee.savi.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by savi on 6/6/15.
 */
@ResponseBody
public abstract class CrudRestController<T, ID extends Serializable> {

    @Inject
    protected JpaRepository<T, ID> repository;

    @RequestMapping(method = GET)
    public List<T> list() {
        return repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public T get(@PathVariable ID id) {
        return repository.findOne(id);
    }

    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> create(@RequestBody T entity) {
        T created = repository.save(entity);
        return new ResponseEntity<>(created, HttpStatus.CREATED);
    }

    @RequestMapping(method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public T update(@RequestBody T entity) {
        return repository.save(entity);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public void delete(@PathVariable ID id) {
        repository.delete(id);
    }
}
