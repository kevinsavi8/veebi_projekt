package ee.savi.controller;

import ee.savi.model.SubjectAttributeType;
import ee.savi.service.SubjectAttributeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by savi on 6/7/15.
 */
@RestController
@RequestMapping("/subject-attribute-types")
public class SubjectAttributeTypeController {

    @Autowired
    private SubjectAttributeTypeService subjectAttributeTypeService;

    @RequestMapping(method = RequestMethod.GET)
    public List<SubjectAttributeType> getAll() {
        return subjectAttributeTypeService.getAll();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public SubjectAttributeType get(@PathVariable Long id) {
        return subjectAttributeTypeService.get(id);
    }

    @RequestMapping(value = "/person",method = RequestMethod.GET)
    public List<SubjectAttributeType> getPersonAttributes() {
        return subjectAttributeTypeService.getPersonAttributes();
    }

    @RequestMapping(value = "/enterprise",method = RequestMethod.GET)
    public List<SubjectAttributeType> getEnterpriseAttributes() {
        return subjectAttributeTypeService.getEnterpriseAttributes();
    }

    @RequestMapping(value = "/employee",method = RequestMethod.GET)
    public List<SubjectAttributeType> getEmployeeAttributes() {
        return subjectAttributeTypeService.getEmployeeAttributes();
    }

    @RequestMapping(value = "/customer",method = RequestMethod.GET)
    public List<SubjectAttributeType> getCustomerAttributes() {
        return subjectAttributeTypeService.getCustomerAttributes();
    }
}
