package ee.savi.controller;

import ee.savi.model.Person;
import ee.savi.service.EnterpriseService;
import ee.savi.service.PersonService;
import ee.savi.model.Criteria;
import ee.savi.model.Enterprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by savi on 6/8/15.
 */
@RestController
@RequestMapping("/search-entities")
public class SearchController {

    @Autowired
    private PersonService personService;
    @Autowired
    private EnterpriseService enterpriseService;

    @RequestMapping(value = "/enterprise",method = RequestMethod.POST)
    public List<Enterprise> getEnterpriseAttributes(@RequestBody Criteria criteria) {
        return enterpriseService.getSearchItems(criteria);
    }

    @RequestMapping(value = "/person",method = RequestMethod.POST)
    public List<Person> getPersonAttributes(@RequestBody Criteria criteria) {
        return personService.getSearchItems(criteria);
    }

    /*@RequestMapping(value = "/employee",method = RequestMethod.GET)
    public List<Employee> getEmployeeAttributes() {
        return subjectAttributeTypeService.getEmployeeAttributes();
    }

    @RequestMapping(value = "/customer",method = RequestMethod.GET)
    public List<Customer> getCustomerAttributes() {
        return subjectAttributeTypeService.getCustomerAttributes();
    }*/
}
