package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public class AddressType extends BaseEntity {

    private Long addressTypeId;
    private String typename;

    public Long getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(Long addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
