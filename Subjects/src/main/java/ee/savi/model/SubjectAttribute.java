package ee.savi.model;

import java.util.Date;

/**
 * Created by savi on 6/7/15.
 */
public class SubjectAttribute extends SubjectBaseEntity {

    private Long subjectAttributeId;
    private Long SubjectAttributeTypeId;
    private Long orderBy;
    private String valueText;
    private Long valueNumber;
    private Date valueDate;
    private Long dataType;

    public Long getSubjectAttributeId() {
        return subjectAttributeId;
    }

    public void setSubjectAttributeId(Long subjectAttributeId) {
        this.subjectAttributeId = subjectAttributeId;
    }

    public Long getSubjectAttributeTypeId() {
        return SubjectAttributeTypeId;
    }

    public void setSubjectAttributeTypeId(Long subjectAttributeTypeId) {
        SubjectAttributeTypeId = subjectAttributeTypeId;
    }

    public Long getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public Long getValueNumber() {
        return valueNumber;
    }

    public void setValueNumber(Long valueNumber) {
        this.valueNumber = valueNumber;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public Long getDataType() {
        return dataType;
    }

    public void setDataType(Long dataType) {
        this.dataType = dataType;
    }
}
