package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public class EmployeeRole extends BaseEntity {

    private Long employeeRoleId;
    private Long employeeId;
    private Long employeeRoleTypeId;
    private String employeeRoleTypeName;
    private String active;

    public Long getEmployeeRoleId() {
        return employeeRoleId;
    }

    public void setEmployeeRoleId(Long employeeRoleId) {
        this.employeeRoleId = employeeRoleId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getEmployeeRoleTypeId() {
        return employeeRoleTypeId;
    }

    public void setEmployeeRoleTypeId(Long employeeRoleTypeId) {
        this.employeeRoleTypeId = employeeRoleTypeId;
    }

    public String getEmployeeRoleTypeName() {
        return employeeRoleTypeName;
    }

    public void setEmployeeRoleTypeName(String employeeRoleTypeName) {
        this.employeeRoleTypeName = employeeRoleTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
