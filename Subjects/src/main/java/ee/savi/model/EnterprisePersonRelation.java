package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public class EnterprisePersonRelation extends BaseEntity {

    private Long EnterprisePersonRelationId;
    private Long personId;
    private Long enterpriseId;
    private Long relationTypeId;
    private String relationTypeName;

    public Long getEnterprisePersonRelationId() {
        return EnterprisePersonRelationId;
    }

    public void setEnterprisePersonRelationId(Long enterprisePersonRelationId) {
        EnterprisePersonRelationId = enterprisePersonRelationId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public Long getRelationTypeId() {
        return relationTypeId;
    }

    public void setRelationTypeId(Long relationTypeId) {
        this.relationTypeId = relationTypeId;
    }

    public String getRelationTypeName() {
        return relationTypeName;
    }

    public void setRelationTypeName(String relationTypeName) {
        this.relationTypeName = relationTypeName;
    }
}
