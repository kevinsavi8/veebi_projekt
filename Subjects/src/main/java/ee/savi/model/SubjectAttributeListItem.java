package ee.savi.model;

import java.util.Date;

/**
 * Created by savi on 6/8/15.
 */
public class SubjectAttributeListItem extends BaseEntity {

    private Long subjectAttributeTypeId;
    private Long subjectTypeId;
    private String typeName;
    private Long dataType;
    private Long orderBy;
    private String required;
    private Long subjectAttributeId;
    private Long subjectId;
    private String valueText;
    private Long valueNumber;
    private Date valueDate;

    public Long getSubjectAttributeTypeId() {
        return subjectAttributeTypeId;
    }

    public void setSubjectAttributeTypeId(Long subjectAttributeTypeId) {
        this.subjectAttributeTypeId = subjectAttributeTypeId;
    }

    public Long getSubjectTypeId() {
        return subjectTypeId;
    }

    public void setSubjectTypeId(Long subjectTypeId) {
        this.subjectTypeId = subjectTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getDataType() {
        return dataType;
    }

    public void setDataType(Long dataType) {
        this.dataType = dataType;
    }

    public Long getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public Long getSubjectAttributeId() {
        return subjectAttributeId;
    }

    public void setSubjectAttributeId(Long subjectAttributeId) {
        this.subjectAttributeId = subjectAttributeId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public Long getValueNumber() {
        return valueNumber;
    }

    public void setValueNumber(Long valueNumber) {
        this.valueNumber = valueNumber;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }
}
