package ee.savi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by savi on 6/5/15.
 */
@MappedSuperclass
public abstract class IdEntity extends BaseEntity {

    @Id
    @GeneratedValue
    Long id;

    @JsonIgnore
    public boolean isNew() {
        return id == null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
