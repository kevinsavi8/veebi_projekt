package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public class SubjectAttributeType extends BaseEntity {

    private Long subjectAttributeType;
    private Long subjectTypeId;
    private String typeName;
    private Long dataType;
    private Long orderBy;
    private String required;
    private String multipleAttributes;
    private String createdByDefault;

    public Long getSubjectAttributeType() {
        return subjectAttributeType;
    }

    public void setSubjectAttributeType(Long subjectAttributeType) {
        this.subjectAttributeType = subjectAttributeType;
    }

    public Long getSubjectTypeId() {
        return subjectTypeId;
    }

    public void setSubjectTypeId(Long subjectTypeId) {
        this.subjectTypeId = subjectTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getDataType() {
        return dataType;
    }

    public void setDataType(Long dataType) {
        this.dataType = dataType;
    }

    public Long getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getMultipleAttributes() {
        return multipleAttributes;
    }

    public void setMultipleAttributes(String multipleAttributes) {
        this.multipleAttributes = multipleAttributes;
    }

    public String getCreatedByDefault() {
        return createdByDefault;
    }

    public void setCreatedByDefault(String createdByDefault) {
        this.createdByDefault = createdByDefault;
    }
}
