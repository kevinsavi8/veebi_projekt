package ee.savi.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by savi on 6/8/15.
 */
public class Criteria extends BaseEntity {

    private String type;
    private String firstName;
    private String name;
    private String country;
    private String county;
    private String townVillage;
    private String streetAddress;
    private String zipCode;
    private List<CriteriaItem> items;

    public Criteria() {
        this.items = new ArrayList<CriteriaItem>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTownVillage() {
        return townVillage;
    }

    public void setTownVillage(String townVillage) {
        this.townVillage = townVillage;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<CriteriaItem> getItems() {
        return items;
    }

    public void setItems(List<CriteriaItem> items) {
        this.items = items;
    }
}
