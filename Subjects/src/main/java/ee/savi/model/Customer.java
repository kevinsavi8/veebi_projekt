package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public class Customer extends SubjectBaseEntity {

    private Long customerId;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
}
