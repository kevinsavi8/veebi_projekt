package ee.savi.model;

/**
 * Created by savi on 6/7/15.
 */
public abstract class SubjectBaseEntity extends BaseEntity {

    private Long subjectTypeId;
    private Long subject;

    public Long getSubjectTypeId() {
        return subjectTypeId;
    }

    public void setSubjectTypeId(Long subjectTypeId) {
        this.subjectTypeId = subjectTypeId;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }
}
