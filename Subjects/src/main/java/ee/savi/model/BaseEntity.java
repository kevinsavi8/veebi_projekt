package ee.savi.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by savi on 6/5/15.
 */
@MappedSuperclass
@JsonIdentityInfo(generator = JSOGGenerator.class)
public abstract class BaseEntity implements Serializable {
}
