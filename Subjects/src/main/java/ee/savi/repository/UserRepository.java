package ee.savi.repository;

import ee.savi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by savi on 6/6/15.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
