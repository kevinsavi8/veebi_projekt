package ee.savi.auth;

import ee.savi.model.User;
import ee.savi.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by User on 8.06.2015.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.loadUserByUsername(username);

        if (user == null) {
            System.out.println("UUUUUUSER ON NULLLLL");
            throw new UsernameNotFoundException("Not found");
        }

        return new UserPrincipal(user);
    }
}
