package ee.savi.service;

import ee.savi.model.Criteria;
import ee.savi.model.Enterprise;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface EnterpriseService {

    List<Enterprise> getAll();
    List<Enterprise> getSearchItems(Criteria criteria);
    Enterprise get(Long enterpriseId);
    Long insert(Enterprise enterprise);
}
