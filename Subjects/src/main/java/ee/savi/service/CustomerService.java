package ee.savi.service;

import ee.savi.model.Customer;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface CustomerService {

    List<Customer> getAll();
    Customer get(Long customerId);
    Long insert(Customer customer);
}
