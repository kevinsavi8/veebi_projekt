package ee.savi.service;

import ee.savi.model.Address;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface AddressService {

    List<Address> getAll();
    List<Address> getBySubject(Long subjectId, Long subjectTypeId);
    Address get(Long addressId);
    Long insert(Address address);
    void update(Address address);
}
