package ee.savi.service;

import ee.savi.model.User;

/**
 * Created by User on 9.06.2015.
 */
public interface UserService {
    User loadUserByUsername(String username);
}
