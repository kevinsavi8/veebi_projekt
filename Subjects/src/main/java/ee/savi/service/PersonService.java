package ee.savi.service;

import ee.savi.model.Criteria;
import ee.savi.model.Person;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface PersonService {

    List<Person> getAll();
    List<Person> getSearchItems(Criteria criteria);
    Person get(Long personId);
    Long insert(Person person);
}
