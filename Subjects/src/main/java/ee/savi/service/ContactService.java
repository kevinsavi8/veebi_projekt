package ee.savi.service;

import ee.savi.model.Contact;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface ContactService {

    List<Contact> getAll();
}
