package ee.savi.service;

import ee.savi.model.SubjectAttributeType;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface SubjectAttributeTypeService {

    List<SubjectAttributeType> getAll();
    SubjectAttributeType get(Long subjectAttributeId);
    List<SubjectAttributeType> getPersonAttributes();
    List<SubjectAttributeType> getEnterpriseAttributes();
    List<SubjectAttributeType> getEmployeeAttributes();
    List<SubjectAttributeType> getCustomerAttributes();
}
