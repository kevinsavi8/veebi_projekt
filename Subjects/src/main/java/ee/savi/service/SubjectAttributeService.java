package ee.savi.service;

import ee.savi.model.SubjectAttribute;
import ee.savi.model.SubjectAttributeListItem;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
public interface SubjectAttributeService {

    List<SubjectAttribute> getAll();
    SubjectAttribute get(Long subjectAttributeId);
    List<SubjectAttributeListItem> getPersonAttributes(Long subjectId);
    List<SubjectAttributeListItem> getEnterpriseAttributes(Long subjectId);
    List<SubjectAttributeListItem> getEmployeeAttributes(Long subjectId);
    List<SubjectAttributeListItem> getCustomerAttributes(Long subjectId);
    Long insert(SubjectAttribute subjectAttribute);
}
