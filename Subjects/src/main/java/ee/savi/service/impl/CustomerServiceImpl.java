package ee.savi.service.impl;

import ee.savi.dao.CustomerDAO;
import ee.savi.model.Customer;
import ee.savi.service.CustomerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Resource
    private CustomerDAO customerDAO;

    @Override
    public List<Customer> getAll() {
        return customerDAO.getAll();
    }

    @Override
    public Customer get(Long customerId) {
        return customerDAO.get(customerId);
    }

    @Override
    public Long insert(Customer customer) {
        return customerDAO.insert(customer);
    }
}
