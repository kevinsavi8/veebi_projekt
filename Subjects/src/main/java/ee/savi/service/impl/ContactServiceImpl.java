package ee.savi.service.impl;

import ee.savi.dao.ContactDAO;
import ee.savi.model.Contact;
import ee.savi.service.ContactService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Resource
    private ContactDAO contactDAO;

    @Override
    public List<Contact> getAll() {
        return contactDAO.getAll();
    }
}
