package ee.savi.service.impl;

import ee.savi.dao.PersonDAO;
import ee.savi.model.Criteria;
import ee.savi.model.Person;
import ee.savi.service.PersonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Resource
    private PersonDAO personDAO;

    @Override
    public List<Person> getAll() {
        return personDAO.getAll();
    }

    @Override
    public List<Person> getSearchItems(Criteria criteria) {
        return personDAO.getSearchItems(criteria);
    }

    @Override
    public Person get(Long personId) {
        return personDAO.get(personId);
    }

    @Override
    public Long insert(Person person) {
        return personDAO.insert(person);
    }
}
