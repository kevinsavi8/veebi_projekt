package ee.savi.service.impl;

import ee.savi.dao.AddressDAO;
import ee.savi.model.Address;
import ee.savi.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class AddressServiceImpl implements AddressService {

    @Resource
    private AddressDAO addressDAO;

    @Override
    public List<Address> getAll() {
        return addressDAO.getAll();
    }

    @Override
    public List<Address> getBySubject(Long subjectId, Long subjectTypeId) {
        return addressDAO.getBySubject(subjectId, subjectTypeId);
    }

    @Override
    public Address get(Long addressId) {
        return addressDAO.get(addressId);
    }

    @Override
    public Long insert(Address address) {
        return addressDAO.insert(address);
    }

    @Override
    public void update(Address address) {
        addressDAO.update(address);
    }
}
