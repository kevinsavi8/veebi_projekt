package ee.savi.service.impl;

import ee.savi.dao.EnterpriseDAO;
import ee.savi.model.Criteria;
import ee.savi.model.Enterprise;
import ee.savi.service.EnterpriseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    @Resource
    private EnterpriseDAO enterpriseDAO;

    @Override
    public List<Enterprise> getAll() {
        return enterpriseDAO.getAll();
    }
    public List<Enterprise> getSearchItems(Criteria criteria) {
        return enterpriseDAO.getSearchItems(criteria);
    }

    @Override
    public Enterprise get(Long enterpriseId) {
        return enterpriseDAO.get(enterpriseId);
    }

    @Override
    public Long insert(Enterprise enterprise) {
        return enterpriseDAO.insert(enterprise);
    }
}
