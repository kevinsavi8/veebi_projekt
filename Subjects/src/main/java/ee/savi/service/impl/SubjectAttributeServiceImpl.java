package ee.savi.service.impl;

import ee.savi.dao.SubjectAttributeDAO;
import ee.savi.model.SubjectAttribute;
import ee.savi.model.SubjectAttributeListItem;
import ee.savi.service.SubjectAttributeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class SubjectAttributeServiceImpl implements SubjectAttributeService {

    @Resource
    private SubjectAttributeDAO subjectAttributeDAO;

    @Override
    public List<SubjectAttribute> getAll() {
        return subjectAttributeDAO.getAll();
    }

    @Override
    public SubjectAttribute get(Long subjectAttributeId) {
        return subjectAttributeDAO.get(subjectAttributeId);
    }

    @Override
    public List<SubjectAttributeListItem> getPersonAttributes(Long subjectId) {
        return subjectAttributeDAO.getPersonById(subjectId);
    }

    @Override
    public List<SubjectAttributeListItem> getEnterpriseAttributes(Long subjectId) {
        return subjectAttributeDAO.getEnterpriseById(subjectId);
    }

    @Override
    public List<SubjectAttributeListItem> getEmployeeAttributes(Long subjectId) {
        return subjectAttributeDAO.getEmployeeById(subjectId);
    }

    @Override
    public List<SubjectAttributeListItem> getCustomerAttributes(Long subjectId) {
        return subjectAttributeDAO.getCustomerById(subjectId);
    }

    @Override
    public Long insert(SubjectAttribute subjectAttribute) {
        return subjectAttributeDAO.insert(subjectAttribute);
    }
}
