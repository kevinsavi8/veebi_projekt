package ee.savi.service.impl;

import ee.savi.dao.SubjectAttributeTypeDAO;
import ee.savi.model.SubjectAttributeType;
import ee.savi.service.SubjectAttributeTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Service
public class SubjectAttributeTypeServiceImpl implements SubjectAttributeTypeService {

    @Resource
    private SubjectAttributeTypeDAO subjectAttributeTypeDAO;

    @Override
    public List<SubjectAttributeType> getAll() {
        return subjectAttributeTypeDAO.getAll();
    }

    @Override
    public SubjectAttributeType get(Long subjectAttributeId) {
        return subjectAttributeTypeDAO.get(subjectAttributeId);
    }

    @Override
    public List<SubjectAttributeType> getPersonAttributes() {
        return subjectAttributeTypeDAO.getPersonAttributes();
    }

    @Override
    public List<SubjectAttributeType> getEnterpriseAttributes() {
        return subjectAttributeTypeDAO.getEnterpriseAttributes();
    }

    @Override
    public List<SubjectAttributeType> getEmployeeAttributes() {
        return subjectAttributeTypeDAO.getEmployeeAttributes();
    }

    @Override
    public List<SubjectAttributeType> getCustomerAttributes() {
        return subjectAttributeTypeDAO.getCustomerAttributes();
    }
}
