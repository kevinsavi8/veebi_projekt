package ee.savi.service.impl;

import ee.savi.dao.UserDAO;
import ee.savi.model.User;
import ee.savi.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by User on 9.06.2015.
 */

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDAO userDao;

    @Override
    public User loadUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }
}
