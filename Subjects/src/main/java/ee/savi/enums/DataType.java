package ee.savi.enums;

/**
 * Created by savi on 6/7/15.
 */
public enum DataType {
    STRING(1L), NUMBER(2L), DATE(3L);
    private Long value;

    DataType(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}
