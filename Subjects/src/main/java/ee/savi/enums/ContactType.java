package ee.savi.enums;

/**
 * Created by savi on 6/7/15.
 */
public enum ContactType {

    EMAIL(1L),PHONE(2L);
    private Long value;

    ContactType(Long value) {
        this.value = value;
    }
}
