package ee.savi.enums;

/**
 * Created by savi on 6/7/15.
 */
public enum SubjectType {
    PERSON(1L),ENTERPRISE(2L),EMPLOYEE(3L),CUSTOMER(4L);
    private Long value;

    SubjectType(Long value) {
        this.value = value;
    }
}
