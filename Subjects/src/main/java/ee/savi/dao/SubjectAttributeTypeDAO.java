package ee.savi.dao;

import ee.savi.dao.rowmapper.SubjectAttributeTypeRowMapper;
import ee.savi.model.SubjectAttributeType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class SubjectAttributeTypeDAO extends BaseDAO {

    public List<SubjectAttributeType> getAll() {
        String sql = "select * from subject_attribute_type";
        return getJdbcTemplate().query(sql, new SubjectAttributeTypeRowMapper());
    }

    public SubjectAttributeType get(Long subjectAttributeId) {
        String sql = "select * from subject_attribute_type where subject_attribute_type=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{subjectAttributeId}, new SubjectAttributeTypeRowMapper());
    }

    public List<SubjectAttributeType> getPersonAttributes() {
        String sql = "select * from subject_attribute_type where subject_type_fk=1";
        return getJdbcTemplate().query(sql, new SubjectAttributeTypeRowMapper());
    }

    public List<SubjectAttributeType> getEnterpriseAttributes() {
        String sql = "select * from subject_attribute_type where subject_type_fk=2";
        return getJdbcTemplate().query(sql, new SubjectAttributeTypeRowMapper());
    }

    public List<SubjectAttributeType> getEmployeeAttributes() {
        String sql = "select * from subject_attribute_type where subject_type_fk=3";
        return getJdbcTemplate().query(sql, new SubjectAttributeTypeRowMapper());
    }

    public List<SubjectAttributeType> getCustomerAttributes() {
        String sql = "select * from subject_attribute_type where subject_type_fk=4";
        return getJdbcTemplate().query(sql, new SubjectAttributeTypeRowMapper());
    }

}
