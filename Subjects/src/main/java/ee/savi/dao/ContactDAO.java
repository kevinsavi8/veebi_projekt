package ee.savi.dao;

import ee.savi.dao.rowmapper.ContactRowMapper;
import ee.savi.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class ContactDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Contact> getAll() {
        String sql = "SELECT contact,subject_fk,contact_type_fk,value_text,orderby,subject_type_fk,note FROM contact";
        return jdbcTemplate.query(sql, new ContactRowMapper());
    }
}
