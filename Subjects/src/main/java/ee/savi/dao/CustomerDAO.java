package ee.savi.dao;

import ee.savi.dao.rowmapper.CustomerRowMapper;
import ee.savi.model.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class CustomerDAO extends BaseDAO {

    public List<Customer> getAll() {
        String sql = "select c.customer, c.subject_fk, c.subject_type_fk from customer c";
        return getJdbcTemplate().query(sql, new CustomerRowMapper());
    }

    public Customer get(Long personId) {
        String sql = "select c.customer, c.subject_fk, c.subject_type_fk from customer c where c.customer=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{personId}, new CustomerRowMapper());
    }

    public Long insert(Customer customer) {
        String sql = "insert into customer (customer,subject_fk,subject_type_fk) " +
                "values (?,?,?) returning customer";
        int newId = getJdbcTemplate().queryForObject(sql, new Object[]{customer.getCustomerId(), customer.getSubject(), customer.getSubjectTypeId()}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("customer");
            }
        });
        return Long.valueOf(newId);
    }
}
