package ee.savi.dao;

import ee.savi.model.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 9.06.2015.
 */

@Repository
public class UserDAO extends BaseDAO {

    public User getUserByUsername(String username) {
        String sql = "SELECT E.employee,UA.user_account, UA.username, UA.passw, P.first_name, P.last_name FROM employee E INNER JOIN user_account UA ON E.employee = UA.subject_fk " +
                "INNER JOIN person P ON E.person_fk = P.person " +
                "WHERE UA.subject_type_fk = 3  AND UA.username=?";
        return getJdbcTemplate().queryForObject(sql, new Object[] {username}, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet rs, int i) throws SQLException {
                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("passw"));
                return user;
            }
        });
    }
}
