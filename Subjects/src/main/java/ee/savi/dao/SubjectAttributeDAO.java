package ee.savi.dao;

import ee.savi.dao.rowmapper.SubjectAttributeListItemRowMapper;
import ee.savi.dao.rowmapper.SubjectAttributeRowMapper;
import ee.savi.model.SubjectAttribute;
import ee.savi.model.SubjectAttributeListItem;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class SubjectAttributeDAO extends BaseDAO {

    public List<SubjectAttribute> getAll() {
        String sql = "select * from subject_attribute";
        return getJdbcTemplate().query(sql, new SubjectAttributeRowMapper());
    }

    public List<SubjectAttributeListItem> getListItems() {
        String sql = "select sat.subject_attribute_type, sat.subject_type_fk, sat.type_name, sat.data_type, sat.orderby, sa.subject_attribute, sa.subject_fk, sa.value_text, sa.value_number, sa.value_date from subject_attribute_type sat inner join subject_attribute sa on sat.subject_attribute_type=sa.subject_attribute_type_fk";
        return getJdbcTemplate().query(sql, new SubjectAttributeListItemRowMapper());
    }

    public List<SubjectAttributeListItem> getPersonById(Long subjectId) {
        String sql = "select sat.subject_attribute_type, sat.subject_type_fk, sat.type_name, sat.data_type, sat.orderby, sa.subject_attribute, sa.subject_fk, sa.value_text, sa.value_number, sa.value_date " +
                "from subject_attribute_type sat " +
                "inner join subject_attribute sa on sat.subject_attribute_type=sa.subject_attribute_type_fk " +
                "where sa.subject_fk=? and sat.subject_type_fk=1";
        return getJdbcTemplate().query(sql, new Object[] {subjectId}, new SubjectAttributeListItemRowMapper());
    }

    public List<SubjectAttributeListItem> getEnterpriseById(Long subjectId) {
        String sql = "select sat.subject_attribute_type, sat.subject_type_fk, sat.type_name, sat.data_type, sat.orderby, sa.subject_attribute, sa.subject_fk, sa.value_text, sa.value_number, sa.value_date " +
                "from subject_attribute_type sat " +
                "inner join subject_attribute sa on sat.subject_attribute_type=sa.subject_attribute_type_fk " +
                "where sa.subject_fk=? and sat.subject_type_fk=2";
        return getJdbcTemplate().query(sql, new Object[] {subjectId}, new SubjectAttributeListItemRowMapper());
    }

    public List<SubjectAttributeListItem> getEmployeeById(Long subjectId) {
        String sql = "select sat.subject_attribute_type, sat.subject_type_fk, sat.type_name, sat.data_type, sat.orderby, sa.subject_attribute, sa.subject_fk, sa.value_text, sa.value_number, sa.value_date " +
                "from subject_attribute_type sat " +
                "inner join subject_attribute sa on sat.subject_attribute_type=sa.subject_attribute_type_fk " +
                "where sa.subject_fk=? and sat.subject_type_fk=3";
        return getJdbcTemplate().query(sql, new Object[] {subjectId}, new SubjectAttributeListItemRowMapper());
    }

    public List<SubjectAttributeListItem> getCustomerById(Long subjectId) {
        String sql = "select sat.subject_attribute_type, sat.subject_type_fk, sat.type_name, sat.data_type, sat.orderby, sa.subject_attribute, sa.subject_fk, sa.value_text, sa.value_number, sa.value_date " +
                "from subject_attribute_type sat " +
                "inner join subject_attribute sa on sat.subject_attribute_type=sa.subject_attribute_type_fk " +
                "where sa.subject_fk=? and sat.subject_type_fk=4";
        return getJdbcTemplate().query(sql, new Object[] {subjectId}, new SubjectAttributeListItemRowMapper());
    }

    public SubjectAttribute get(Long subjectAttributeId) {
        String sql = "select * from subject_attribute where subject_attribute=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{subjectAttributeId}, new SubjectAttributeRowMapper());
    }

    public Long insert(SubjectAttribute subjectAttribute) {
        String sql = "insert into subject_attribute (subject_fk, subject_type_fk, subject_attribute_type_fk, data_type, orderby, " +
                "value_text, value_number, value_date) " +
                "values (?,?,?,?,?,?,?,?) returning subject_attribute";
        int newId = getJdbcTemplate().queryForObject(sql, new Object[]{subjectAttribute.getSubject(), subjectAttribute.getSubjectTypeId(), subjectAttribute.getSubjectAttributeTypeId(),
                subjectAttribute.getDataType(), subjectAttribute.getOrderBy(), subjectAttribute.getValueText(), subjectAttribute.getValueNumber(), subjectAttribute.getValueDate()}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("subject_attribute");
            }
        });
        return Long.valueOf(newId);
    }
}
