package ee.savi.dao;

import ee.savi.dao.rowmapper.EnterpriseRowMapper;
import ee.savi.enums.DataType;
import ee.savi.model.Criteria;
import ee.savi.model.CriteriaItem;
import ee.savi.model.Enterprise;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class EnterpriseDAO extends BaseDAO {

    public List<Enterprise> getAll() {
        String sql = "select e.enterprise, e.name, e.full_name, e.created_by, e.updated_by, e.created, e.updated from enterprise e";
        return getJdbcTemplate().query(sql, new EnterpriseRowMapper());
    }

    public List<Enterprise> getSearchItems(Criteria criteria) {
        List<Object> params = new ArrayList<Object>();
        String sql = "select distinct e.enterprise, e.name, e.full_name, e.created_by, e.updated_by, e.created, e.updated " +
                "from enterprise e inner join address a on e.enterprise=a.subject_fk and a.subject_type_fk=2 ";
        if (StringUtils.isNotEmpty(criteria.getName())) {
            sql += " and upper(name) like upper('%'||?||'%') ";
            params.add(criteria.getName());
        }
        if (StringUtils.isNotEmpty(criteria.getCountry())) {
            sql += " and upper(country) like upper('%'||?||'%') ";
            params.add(criteria.getCountry());
        }
        if (StringUtils.isNotEmpty(criteria.getCounty())) {
            sql += " and upper(county) like upper('%'||?||'%') ";
            params.add(criteria.getCounty());
        }
        if (StringUtils.isNotEmpty(criteria.getTownVillage())) {
            sql += " and upper(town_village) like upper('%'||?||'%') ";
            params.add(criteria.getTownVillage());
        }
        if (StringUtils.isNotEmpty(criteria.getStreetAddress())) {
            sql += " and upper(street_address) like upper('%'||?||'%') ";
            params.add(criteria.getStreetAddress());
        }
        if (StringUtils.isNotEmpty(criteria.getZipCode())) {
            sql += " and upper(zipcode) like upper('%'||?||'%') ";
            params.add(criteria.getZipCode());
        }
        if (!criteria.getItems().isEmpty()) {
            sql += "inner join subject_attribute sa on e.enterprise=sa.subject_fk and sa.subject_type_fk=2 " +
                    "inner join subject_attribute_type sat on sa.subject_attribute_type_fk=sat.subject_attribute_type";
            for (CriteriaItem c : criteria.getItems()) {
                if (DataType.STRING.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and upper(value_text) like upper('%'||?||'%') ";
                    params.add(c.getValueText());
                } else if (DataType.NUMBER.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and value_number = ? ";
                    params.add(c.getValueNumber());
                } else if (DataType.DATE.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and value_date = ? ";
                    params.add(c.getValueNumber());
                }
            }
        }
        return getJdbcTemplate().query(sql, params.toArray(), new EnterpriseRowMapper());
    }

    public Enterprise get(Long enterpriseId) {
        String sql = "select e.enterprise, e.name, e.full_name, e.created_by, e.updated_by, e.created, e.updated " +
                "from enterprise e where e.enterprise=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{enterpriseId}, new EnterpriseRowMapper());
    }

    public Long insert(Enterprise enterprise) {
        String sql = "insert into enterprise (name, full_name, created_by, updated_by, created, updated) " +
                "values (?,?,?,?,current_timestamp,?) returning enterprise";
        int newId = getJdbcTemplate().queryForObject(sql, new Object[]{ enterprise.getName(), enterprise.getFullName(), enterprise.getCreatedBy(), enterprise.getUpdatedBy(), enterprise.getUpdated()}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("enterprise");
            }
        });
        return Long.valueOf(newId);
    }
}
