package ee.savi.dao;

import ee.savi.dao.rowmapper.PersonRowMapper;
import ee.savi.enums.DataType;
import ee.savi.model.Criteria;
import ee.savi.model.CriteriaItem;
import ee.savi.model.Person;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class PersonDAO extends BaseDAO {

    public List<Person> getAll() {
        String sql = "select p.person, p.first_name, p.last_name, p.identity_code, p.birth_date, p.created_by, p.updated_by, p.created, p.updated from person p";
        return getJdbcTemplate().query(sql, new PersonRowMapper());
    }

    public List<Person> getSearchItems(Criteria criteria) {
        List<Object> params = new ArrayList<Object>();
        String sql = "select distinct p.person, p.first_name, p.last_name, p.identity_code, p.birth_date, p.created_by, p.updated_by, p.created, p.updated " +
                "from person p inner join address a on p.person=a.subject_fk and a.subject_type_fk=1 ";
        if (StringUtils.isNotEmpty(criteria.getFirstName())) {
            sql += " and upper(p.first_name) like upper('%'||?||'%') ";
            params.add(criteria.getFirstName());
        }
        if (StringUtils.isNotEmpty(criteria.getName())) {
            sql += " and upper(p.last_name) like upper('%'||?||'%') ";
            params.add(criteria.getName());
        }
        if (StringUtils.isNotEmpty(criteria.getCountry())) {
            sql += " and upper(a.country) like upper('%'||?||'%') ";
            params.add(criteria.getCountry());
        }
        if (StringUtils.isNotEmpty(criteria.getCounty())) {
            sql += " and upper(a.county) like upper('%'||?||'%') ";
            params.add(criteria.getCounty());
        }
        if (StringUtils.isNotEmpty(criteria.getTownVillage())) {
            sql += " and upper(a.town_village) like upper('%'||?||'%') ";
            params.add(criteria.getTownVillage());
        }
        if (StringUtils.isNotEmpty(criteria.getStreetAddress())) {
            sql += " and upper(a.street_address) like upper('%'||?||'%') ";
            params.add(criteria.getStreetAddress());
        }
        if (StringUtils.isNotEmpty(criteria.getZipCode())) {
            sql += " and upper(a.zipcode) like upper('%'||?||'%') ";
            params.add(criteria.getZipCode());
        }
        if (criteria.getItems().size() != 0) {
            sql += "inner join subject_attribute sa on p.person=sa.subject_fk and sa.subject_type_fk=1 " +
                    "inner join subject_attribute_type sat on sa.subject_attribute_type_fk=sat.subject_attribute_type";
            for (CriteriaItem c : criteria.getItems()) {
                if (DataType.STRING.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and upper(value_text) like upper('%'||?||'%') ";
                    params.add(c.getValueText());
                } else if (DataType.NUMBER.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and value_number = ? ";
                    params.add(c.getValueNumber());
                } else if (DataType.DATE.getValue().equals(c.getType())) {
                    sql += " and upper(type_name) like upper('%'||?||'%') ";
                    params.add(c.getName());
                    sql += " and value_date = ? ";
                    params.add(c.getValueNumber());
                }
            }
        }
        return getJdbcTemplate().query(sql, params.toArray(), new PersonRowMapper());
    }

    public Person get(Long personId) {
        String sql = "select p.person, p.first_name, p.last_name, p.identity_code, p.birth_date, p.created_by, p.updated_by, p.created, p.updated " +
                "from person p where p.person=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{personId}, new PersonRowMapper());
    }

    public Long insert(Person person) {
        String sql = "insert into person (first_name, last_name, identity_code, birth_date, created_by, updated_by, created, updated) " +
                "values (?,?,?,?,?,?,current_timestamp,?) returning person";
        int newId = getJdbcTemplate().queryForObject(sql, new Object[]{person.getFirstName(), person.getLastName(), person.getIdentityCode(),
                person.getBirthDate(), person.getCreatedBy(), person.getUpdatedBy(), person.getUpdated()}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("person");
            }
        });
        return Long.valueOf(newId);
    }
}
