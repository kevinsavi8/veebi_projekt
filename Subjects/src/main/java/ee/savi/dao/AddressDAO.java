package ee.savi.dao;

import ee.savi.dao.rowmapper.AddressRowMapper;
import ee.savi.model.Address;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by savi on 6/7/15.
 */
@Repository
public class AddressDAO extends BaseDAO {

    public List<Address> getAll() {
        String sql = "select a.address, a.address_type_fk, a.subject_fk, a.subject_type_fk," +
                " a.country, a.county, a.town_village, a.street_address, a.zipcode from address a";
        return getJdbcTemplate().query(sql, new AddressRowMapper());
    }

    public List<Address> getBySubject(Long subjectId, Long subjectTypeId) {
        String sql = "select a.address, a.address_type_fk, a.subject_fk, a.subject_type_fk," +
                " a.country, a.county, a.town_village, a.street_address, a.zipcode from address a where a.subject_fk=? and a.subject_type_fk=?";
        return getJdbcTemplate().query(sql, new Object[] {subjectId, subjectTypeId}, new AddressRowMapper());
    }

    public Address get(Long addressId) {
        String sql = "select a.address, a.address_type_fk, a.subject_fk, a.subject_type_fk, a.country, a.county, " +
                "a.town_village, a.street_address, a.zipcode from address a where a.address=?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{addressId}, new AddressRowMapper());
    }

    public Long insert(Address address) {
        String sql = "insert into address (address_type_fk, subject_fk, subject_type_fk, country, county, town_village, street_address, zipcode) " +
                "values (?,?,?,?,?,?,?,?) returning address";
        int newId = getJdbcTemplate().queryForObject(sql, new Object[]{ address.getAddressTypeId(), address.getSubject(), address.getSubjectTypeId(), address.getCountry(), address.getCounty()
        , address.getTownVillage(), address.getStreetAddress(), address.getZipCode()}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("address");
            }
        });
        return Long.valueOf(newId);
    }

    public void update(Address address) {
        String sql = "update address set address_type_fk=?, subject_fk=?, subject_type_fk=?, country=?, county=?, town_village=?, street_address=?, zipcode=? where " +
                "address=?";
        getJdbcTemplate().update(sql, new Object[]{ address.getAddressTypeId(), address.getSubject(), address.getSubjectTypeId(), address.getCountry(), address.getCounty()
                , address.getTownVillage(), address.getStreetAddress(), address.getZipCode(), address.getAddressId()});
    }
}
