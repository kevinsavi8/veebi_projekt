package ee.savi.dao.rowmapper;

import ee.savi.model.Enterprise;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by savi on 6/7/15.
 */
public class EnterpriseRowMapper implements RowMapper<Enterprise> {

    @Override
    public Enterprise mapRow(ResultSet resultSet, int i) throws SQLException {
        Enterprise enterprise = new Enterprise();
        enterprise.setEnterpriseId(SqlUtil.getLong(resultSet, "enterprise"));
        enterprise.setName(resultSet.getString("name"));
        enterprise.setFullName(resultSet.getString("full_name"));
        enterprise.setCreatedBy(SqlUtil.getLong(resultSet, "created_by"));
        enterprise.setUpdatedBy(SqlUtil.getLong(resultSet, "updated_by"));
        enterprise.setCreated(resultSet.getTimestamp("created"));
        enterprise.setUpdated(resultSet.getTimestamp("updated"));
        return enterprise;
    }
}
