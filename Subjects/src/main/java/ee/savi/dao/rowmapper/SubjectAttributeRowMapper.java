package ee.savi.dao.rowmapper;

import ee.savi.model.SubjectAttribute;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class SubjectAttributeRowMapper implements RowMapper<SubjectAttribute> {
    @Override
    public SubjectAttribute mapRow(ResultSet resultSet, int i) throws SQLException {
        SubjectAttribute item = new SubjectAttribute();
        item.setSubjectAttributeId(SqlUtil.getLong(resultSet, "subject_attribute"));
        item.setSubject(SqlUtil.getLong(resultSet, "subject_fk"));
        item.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        item.setSubjectAttributeTypeId(SqlUtil.getLong(resultSet, "subject_attribute_type_fk"));
        item.setDataType(SqlUtil.getLong(resultSet, "data_type"));
        item.setOrderBy(SqlUtil.getLong(resultSet, "orderby"));
        item.setValueText(resultSet.getString("value_text"));
        item.setValueNumber(SqlUtil.getLong(resultSet, "value_number"));
        item.setValueDate(resultSet.getTimestamp("value_date"));
        return item;
    }
}
