package ee.savi.dao.rowmapper;

import ee.savi.model.Contact;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class ContactRowMapper implements RowMapper<Contact> {

    @Override
    public Contact mapRow(ResultSet resultSet, int i) throws SQLException {
        Contact contact = new Contact();
        contact.setContactId(SqlUtil.getLong(resultSet, "contact"));
        contact.setContactTypeId(SqlUtil.getLong(resultSet, "contact_type_fk"));
        contact.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        contact.setSubject(SqlUtil.getLong(resultSet, "subject_fk"));
        contact.setContactText(resultSet.getString("value_text"));
        contact.setOrderBy(SqlUtil.getLong(resultSet, "orderby"));
        contact.setNote(resultSet.getString("note"));
        return contact;
    }
}
