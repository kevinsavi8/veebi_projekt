package ee.savi.dao.rowmapper;

import ee.savi.model.SubjectAttributeType;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class SubjectAttributeTypeRowMapper implements RowMapper<SubjectAttributeType> {
    @Override
    public SubjectAttributeType mapRow(ResultSet resultSet, int i) throws SQLException {
        SubjectAttributeType item = new SubjectAttributeType();
        item.setSubjectAttributeType(SqlUtil.getLong(resultSet, "subject_attribute_type"));
        item.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        item.setTypeName(resultSet.getString("type_name"));
        item.setDataType(SqlUtil.getLong(resultSet, "data_type"));
        item.setOrderBy(SqlUtil.getLong(resultSet, "orderby"));
        item.setRequired(resultSet.getString("required"));
        item.setMultipleAttributes(resultSet.getString("multiple_attributes"));
        item.setCreatedByDefault(resultSet.getString("created_by_default"));
        return item;
    }
}
