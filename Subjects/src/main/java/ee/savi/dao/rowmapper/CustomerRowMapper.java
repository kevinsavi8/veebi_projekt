package ee.savi.dao.rowmapper;

import ee.savi.model.Customer;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class CustomerRowMapper implements RowMapper<Customer> {
    @Override
    public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer();
        customer.setCustomerId(SqlUtil.getLong(resultSet, "customer"));
        customer.setSubject(SqlUtil.getLong(resultSet, "subject_fk"));
        customer.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        return customer;
    }
}
