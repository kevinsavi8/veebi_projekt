package ee.savi.dao.rowmapper;

import ee.savi.model.Employee;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class EmployeeRowMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
        Employee employee = new Employee();
        employee.setEmployeeId(SqlUtil.getLong(resultSet, "employee"));
        employee.setEnterpriseId(SqlUtil.getLong(resultSet, "enterprise_fk"));
        employee.setPersonId(SqlUtil.getLong(resultSet, "person_fk"));
        employee.setActive(resultSet.getString("active"));
        return employee;
    }
}
