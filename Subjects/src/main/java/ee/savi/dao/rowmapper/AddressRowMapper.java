package ee.savi.dao.rowmapper;

import ee.savi.model.Address;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class AddressRowMapper implements RowMapper<Address> {
    @Override
    public Address mapRow(ResultSet resultSet, int i) throws SQLException {
        Address address = new Address();
        address.setAddressId(SqlUtil.getLong(resultSet, "address"));
        address.setSubject(SqlUtil.getLong(resultSet, "subject_fk"));
        address.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        address.setAddressTypeId(SqlUtil.getLong(resultSet, "address_type_fk"));
        address.setCountry(resultSet.getString("country"));
        address.setCounty(resultSet.getString("county"));
        address.setTownVillage(resultSet.getString("town_village"));
        address.setStreetAddress(resultSet.getString("street_address"));
        address.setZipCode(resultSet.getString("zipcode"));
        return address;
    }
}
