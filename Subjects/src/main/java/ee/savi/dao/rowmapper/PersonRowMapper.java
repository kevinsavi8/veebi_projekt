package ee.savi.dao.rowmapper;

import ee.savi.model.Person;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public class PersonRowMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();
        person.setPersonId(SqlUtil.getLong(resultSet, "person"));
        person.setCreatedBy(SqlUtil.getLong(resultSet, "created_by"));
        person.setUpdatedBy(SqlUtil.getLong(resultSet, "updated_by"));
        person.setFirstName(resultSet.getString("first_name"));
        person.setLastName(resultSet.getString("last_name"));
        person.setIdentityCode(resultSet.getString("identity_code"));
        person.setBirthDate(resultSet.getTimestamp("birth_date"));
        person.setCreated(resultSet.getTimestamp("created"));
        person.setUpdated(resultSet.getTimestamp("updated"));
        return person;
    }
}
