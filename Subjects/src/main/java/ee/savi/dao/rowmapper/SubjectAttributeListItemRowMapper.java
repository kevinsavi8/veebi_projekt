package ee.savi.dao.rowmapper;

import ee.savi.model.SubjectAttributeListItem;
import ee.savi.util.SqlUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/8/15.
 */
public class SubjectAttributeListItemRowMapper implements RowMapper<SubjectAttributeListItem> {
    @Override
    public SubjectAttributeListItem mapRow(ResultSet resultSet, int i) throws SQLException {
        SubjectAttributeListItem item = new SubjectAttributeListItem();
        item.setSubjectAttributeTypeId(SqlUtil.getLong(resultSet, "subject_attribute_type"));
        item.setSubjectAttributeId(SqlUtil.getLong(resultSet, "subject_attribute"));
        item.setSubjectTypeId(SqlUtil.getLong(resultSet, "subject_type_fk"));
        item.setSubjectId(SqlUtil.getLong(resultSet, "subject_fk"));
        item.setTypeName(resultSet.getString("type_name"));
        item.setDataType(SqlUtil.getLong(resultSet, "data_type"));
        item.setOrderBy(SqlUtil.getLong(resultSet, "orderby"));
        item.setValueText(resultSet.getString("value_text"));
        item.setValueNumber(SqlUtil.getLong(resultSet, "value_number"));
        item.setValueDate(resultSet.getTimestamp("value_date"));
        return item;
    }
}
