package ee.savi.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import javax.annotation.Resource;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.BeanFactory;

/**
 * Created by savi on 6/7/15.
 */
public final class SpringUtil {

    private SpringUtil() {
        // Ensure that no instance is created
    }

    private static ThreadLocal<BeanFactory> threadLocalBeanFactory = new ThreadLocal<BeanFactory>();

    public static void setBeanFactory(BeanFactory beanFactory) {
        threadLocalBeanFactory.set(beanFactory);
    }

    /**
     * This method should be used only within request thread, as it keeps bean factory in thread local storage, populated
     * during servlet filters chain processing.
     */
    public static BeanFactory getBeanFactory() {
        return threadLocalBeanFactory.get();
    }

    public static void injectBeans(Object destination, Object... beans) {
        Validate.notNull(destination, "Destination should be not null!");
        Validate.noNullElements(beans, "All beans should be not null!");
        for (Object bean : beans) {
            Field field = getField(destination.getClass(), bean);
            validateFieldExists(field, destination, bean);
            validateFieldHasResourceAnnotation(field);
            injectBean(destination, field, bean);
        }
    }

    private static Field getField(Class<? extends Object> clazz, Object bean) {
        Class<? extends Object> currentClazz = clazz;
        while (currentClazz != null) {
            for (Field field : currentClazz.getDeclaredFields()) {
                String fieldCanonicalName = field.getType().getCanonicalName();
                Class<? extends Object> beanClazz = bean.getClass();
                if (beanClassMatches(beanClazz, fieldCanonicalName)) {
                    return field;
                }
                if (beanSuperClassMatches(beanClazz, fieldCanonicalName)) {
                    return field;
                }
                if (beanImplementsFieldInterface(beanClazz, fieldCanonicalName)) {
                    return field;
                }
            }
            currentClazz = currentClazz.getSuperclass();
        }
        return null;
    }

    private static boolean beanClassMatches(Class<? extends Object> beanClazz, String fieldCanonicalName) {
        String beanClassCanonicalName = beanClazz.getCanonicalName();
        return fieldCanonicalName.equals(beanClassCanonicalName);
    }

    private static boolean beanSuperClassMatches(Class<? extends Object> beanClazz, String fieldCanonicalName) {
        String beanSuperClassCanonicalName = beanClazz.getSuperclass().getCanonicalName();
        return fieldCanonicalName.equals(beanSuperClassCanonicalName);
    }

    private static boolean beanImplementsFieldInterface(Class<? extends Object> beanClazz, String fieldCanonicalName) {
        Class<?>[] interfaceClasses = beanClazz.getInterfaces();
        for (Class<?> interfaceClass : interfaceClasses) {
            if (fieldCanonicalName.equals(interfaceClass.getCanonicalName())) {
                return true;
            }
        }
        return false;
    }

    private static void validateFieldExists(Field field, Object destination, Object bean) {
        if (field == null) {
            throw new NoSuchFieldInBeanException(destination.getClass(), bean.getClass());
        }
    }

    private static void validateFieldHasResourceAnnotation(Field field) {
        if (!hasResourceAnnotation(field)) {
            throw new NoSuchFieldAnnotationException(field.getType(), Resource.class);
        }
    }

    private static boolean hasResourceAnnotation(Field field) {
        for (Annotation fieldAnnotation : field.getDeclaredAnnotations()) {
            if (fieldAnnotation instanceof Resource) {
                return true;
            }
        }
        return false;
    }

    private static void injectBean(Object destination, Field field, Object bean) {
        try {
            field.setAccessible(true);
            field.set(destination, bean);
        } catch (IllegalAccessException e) {
            String errorMessage =
                    "Failed to inject bean " + bean.getClass().getSimpleName() + " to class "
                            + destination.getClass().getSimpleName();
            throw new RuntimeException(errorMessage, e);
        }
    }

    public static class NoSuchFieldInBeanException extends RuntimeException {
        public NoSuchFieldInBeanException(Class<?> beanClass, Class<?> fieldClass) {
            super("Bean of type " + beanClass.getCanonicalName() + " does not have field of type "
                    + fieldClass.getClass().getCanonicalName());
        }
    }

    public static class NoSuchFieldAnnotationException extends RuntimeException {
        public NoSuchFieldAnnotationException(Class<?> fieldClass, Class<?> annotationClass) {
            super("Field of type " + fieldClass.getCanonicalName() + " is declared, but does not have  "
                    + annotationClass.getSimpleName() + " annotation");
        }
    }
}
