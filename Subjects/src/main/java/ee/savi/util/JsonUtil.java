package ee.savi.util;


import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by savi on 6/7/15.
 */
public final class JsonUtil {

    public final static String EMPTY_JSON = "{}";
    public final static String EMPTY_JSON_COLLECTION = "[]";
    private final static String DATE_FORMAT = "dd.MM.yyyy HH:mm";

    private JsonUtil() {
        // avoid instances
    }

    public static String asJson(Object toSerialize) {
        if (toSerialize == null) {
            return EMPTY_JSON;
        }
        return getGsonBuilder().toJson(toSerialize);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return fromJson(json, clazz, false);
    }

    /**
     * @param createEmptyInstance if set to true and serialized object string equals to {@link JsonUtil#EMPTY_JSON}, then
     *          new instance of a class is created, otherwise null is returned.
     */
    public static <T> T fromJson(String json, Class<T> clazz, boolean createEmptyInstance) {
        if (StringUtils.isEmpty(json) || (!createEmptyInstance && EMPTY_JSON.equals(json))) {
            return null;
        }
        return getGsonBuilder().fromJson(json, clazz);
    }

    public static <T> T fromJson(String json, TypeToken<T> typeToken) {
        if (StringUtils.isEmpty(json) || EMPTY_JSON.equals(json) || EMPTY_JSON_COLLECTION.equals(json)) {
            return null;
        }
        return getGsonBuilder().<T> fromJson(json, typeToken.getType());
    }

    public static <T> T fromJson(JsonElement json, Class<T> clazz) {
        if (json == null) {
            return null;
        }
        return getGsonBuilder().<T> fromJson(json, clazz);
    }

    public static Gson getGsonBuilder() {
        return new GsonBuilder().setDateFormat(DATE_FORMAT).create();
    }

    public static Gson getISO8601GsonBuilder() {
        return new GsonBuilder().registerTypeAdapter(Date.class, new ISO8601DateAdapter()).create();
    }

    public static String asISO8601Json(Object toSerialize) {
        if (toSerialize == null) {
            return EMPTY_JSON;
        }
        return getISO8601GsonBuilder().toJson(toSerialize);
    }

    public static <T> T fromISO8601Json(String json, Class<T> clazz) {
        if (json == null) {
            return null;
        }
        return getISO8601GsonBuilder().<T> fromJson(json, clazz);
    }

    public static <T> T fromISO8601Json(String json, TypeToken<T> typeToken) {
        if (StringUtils.isEmpty(json) || EMPTY_JSON.equals(json) || EMPTY_JSON_COLLECTION.equals(json)) {
            return null;
        }
        return getISO8601GsonBuilder().<T> fromJson(json, typeToken.getType());
    }

    public static class ISO8601DateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {

        @Override
        public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            String dateString = json.getAsJsonPrimitive().getAsString();
            if (dateString == null) {
                return null;
            }
            return DatatypeConverter.parseDateTime(dateString).getTime();
        }

        @Override
        public JsonElement serialize(Date date, Type type, JsonSerializationContext context) {
            if (date == null) {
                return null;
            }
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return new JsonPrimitive(DatatypeConverter.printDateTime(c));
        }

    }
}
