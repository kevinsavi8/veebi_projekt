package ee.savi.util;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by savi on 6/7/15.
 */
public final class SqlUtil {

    public SqlUtil() {
    }

    public static Long getLong(ResultSet rs, String columnLabel) throws SQLException {
        return getLong(rs.getObject(columnLabel));
    }

    public static Long getLong(Object object) {
        if (object == null) {
            return null;
        } else if (object instanceof BigDecimal) {
            return Long.valueOf(((BigDecimal) object).longValue());
        } else if (object instanceof Long) {
            return (Long) object;
        } else if (object instanceof String) {
            return Long.valueOf((String) object);
        }
        throw new IllegalArgumentException("You can give only String, BigDecimal and Long types!");
    }
}
