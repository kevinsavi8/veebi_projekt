package ee.savi.util;

import ee.savi.auth.UserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by User on 9.06.2015.
 */
public class UserUtil {

    public static String getCurrentUser() {

        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return userPrincipal.getUsername();

    }

}
