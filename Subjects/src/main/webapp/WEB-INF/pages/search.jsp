<%--
	 Created by IntelliJ IDEA.
	 User: savi
	 Date: 6/8/15
	 Time: 2:56 AM
	 To change this template use File | Settings | File Templates.
	 --%>
	<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		<html ng-app="subjects">
			<head>
				<meta charset="utf-8">
				<title>Search</title>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
				<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular.min.js"></script>
				<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
				<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
				<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
				<script>
					var app = angular.module('subjects', []);

					app.controller('SearchController', ['$scope', '$http', function($scope, $http) {

						var entityType;

						$scope.onChange = function(type) {
							entityType = type;
							$http.get('/SAVI/subject-attribute-types/'+type).success(function(data) {
								$scope.attributes = data;
								//$scope.criteria = [];
							});
						};

						$scope.criteria = {};
						$scope.resultPersons = {};
						$scope.resultEnterprises = {};
						$scope.attributeList = [];

						$scope.search = function() {
							$scope.attributes.forEach(function(attribute) {
								if (typeof attribute.reply !== 'undefined') {
									if (attribute.dataType == 1) {
										$scope.attributeList.push({
											'type':1,
											'name':attribute.typeName,
											'valueText':attribute.reply
										});
									} else if (attribute.dataType == 2) {
										$scope.attributeList.push({
											'type':2,
											'name':attribute.typeName,
											'valueNumber':attribute.reply
										});
									} else if (attribute.dataType == 3) {
										$scope.attributeList.push({
											'type':3,
											'name':attribute.typeName,
											'valueDate':attribute.reply
										});
									}
								}
							});
							console.log($scope.attributeList);
							console.log($scope.criteria);
							$scope.criteriaList = $scope.criteria;
							$scope.criteria.items = $scope.attributeList;

							if (entityType == 'enterprise') {
								console.log("Criteria: ",$scope.criteriaList);
								$http.post('/SAVI/search-entities/enterprise', $scope.criteria).success(function(data) {
									console.log("Results: ",data);
									$scope.resultEnterprises = data;
								});
							} else if (entityType == 'person') {
								$http.post('/SAVI/search-entities/person', $scope.criteria).success(function(data) {
									console.log("Results: ",data);
									$scope.resultPersons = data;
								});
							}

						};

					}]);
				</script>
			</head>
			<body>
				<h1 class="text-center">Subjects search</h1>
				<div class="container">
					<ul class="nav nav-tabs">
						<li><a href="./">Home</a></li>
						<li class="active"><a href="search">Search</a></li>
						<li role="presentation" class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
								Edit <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="edit_enterprise/1">Enterprise</a></li>
								<li><a href="edit_person/1">Person</a></li>
							</ul>
						</li>
						<li><a href="logout">Logout</a></li>
					</ul>
				</div>
				<br/>
				<div ng-controller="SearchController">
					<div class="container text-center">
						<label for="searchId">Search for: </label>
						<input id="searchId" type="radio" ng-model="type" value="person" ng-change="onChange(type)" /> Person
						<input id="searchId" type="radio" ng-model="type" value="enterprise" ng-change="onChange(type)" /> Enterprise
					</div>
					<!-- {{criteria}} -->
					<form name="searchForm" role="form" ng-submit="search()">
						<div class="col-sm-offset-3 col-sm-4">
							<div class="form-group" ng-hide="type == 'enterprise'">
								<label for="first_name">First name:</label>
								<input class="form-control" id="first_name" type="text" ng-model="criteria.firstName" />
							</div>
							<div class="form-group">
								<label for="name">Name:</label>
								<input class="form-control" id="name" type="text" ng-model="criteria.name" />
							</div>
							<div class="form-group">
								<label for="country">Country:</label>
								<input class="form-control" id="country"  type="text" ng-model="criteria.country" />
							</div>
							<div class="form-group">
								<label for="county">County:</label>
								<input class="form-control" id="county"  type="text" ng-model="criteria.county" />
							</div>
							<div class="form-group">
								<label for="townVillage">Town/Village:</label>
								<input class="form-control" id="townVillage"  type="text" ng-model="criteria.townVillage" />
							</div>
							<div class="form-group">
								<label for="address">Address:</label>
								<input class="form-control" id="address"  type="text" ng-model="criteria.streetAddress" />
							</div>
							<div class="form-group">
								<label for="zipCode">Postal code:</label>
								<input class="form-control" id="zipCode"  type="text" ng-model="criteria.zipCode" />
							</div>
							<div ng-repeat="attribute in attributes">
								<div class="form-group">
									<label >{{attribute.typeName}}</label>
									<input class="form-control" type="text" ng-model="attribute.reply"/>
								</div>
							</div>
							<input class="btn btn-primary btn-block" type="submit" id="submit" value="Search" />
						</div>
					</form>
					<div class="col-sm-4">
						<h3 style="padding: 0px; margin: 0px;">Search results:</h3>
						<!-- 
						{{resultPersons}}
						{{resultEnterprises}}
						-->
						<div>
							<table>
								<tr ng-repeat="person in resultPersons">
									<td>Isik: {{ person.firstName }}&nbsp;</td>
									<td>{{ person.lastName }}</td>
								</tr>
							</table>
						</div>
						
						<div>
							<table>
								<tr ng-repeat="enterprise in resultEnterprises">
									<td>Ettev�tte t�isnimi: {{ enterprise.fullName }}&nbsp;</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</body>
		</html>
