<%--
  Created by IntelliJ IDEA.
  User: savi
  Date: 6/8/15
  Time: 12:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app>
<head>
  <meta charset="utf-8">
  <title>Edit person</title>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
  <script>
    function PersonEditController($scope, $http, $location) {
      var personId = +$location.absUrl().substring($location.absUrl().lastIndexOf('/')+1);
      if (typeof personId === 'number') {
        $http.get('/SAVI/persons/'+personId).success(function(data) {
          if (!data) {
            $scope.errorMessage = "Could not find person with that id";
          } else {
            $scope.person = data;
            $http.get('/SAVI/addresses/subject/', {
              params : {
                'subjectId':$scope.person.personId,
                'subjectTypeId':1
              }
            }).success(function(data) {
              $scope.personAddresses = data;
            });
          }
        }).error(function (error) {
          $scope.errorMessage = "Could not find person with that id";
          console.log("Could not get person: ", error);
        });
      }

      $scope.replys = [];

      $http.get('/SAVI/subject-attribute-types/person').success(function(data) {
        $scope.attributes = data;
        $http.get('/SAVI/subject-attributes/person/'+personId).success(function(data) {
          $scope.attributesReplys = data;
        });

      });

      $scope.addAnotherAddress = function() {
        $scope.personAddresses.push({
          'addressTypeId' : 3,
          'subject' : personId,
          'subjectTypeId' : 1
        });
      };

      $scope.submitPerson = function() {
        $scope.personAddresses.forEach(function(address) {
          if(address.addressId) {
            $http.put('/SAVI/addresses',address).success(function(data) {
            });
          } else {
            $http.post('/SAVI/addresses',address).success(function(data) {
              address.addressId = data;
            });
          }
        });
        $scope.attributes.forEach(function(attribute) {
          if (attribute.reply !== 'undefined') {
            $scope.attributesReplys.forEach(function(reply) {
              console.log('COOL', reply.subjectAttributeTypeId);
              if (reply.subjectAttributeTypeId == attribute.subjectAttributeTypeId) {
                if (reply.dataType = 1) {
                  reply.valueText = attribute.reply;
                } else if (reply.dataType = 2) {
                  reply.valueNumber = attribute.reply;
                } else {
                  reply.valueDate = attribute.reply;
                }
              }
            });
            console.log($scope.attributesReplys);
          }

        });
      }

    }
  </script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
<div ng-controller="PersonEditController">
<h1 class="text-center">Edit person</h1>
<div class="container">
  <ul class="nav nav-tabs">
    <li><a href="../">Home</a></li>
    <li><a href="../search">Search</a></li>
    <li role="presentation" class="dropdown active">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
        Edit <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="../edit_enterprise/1">Enterprise</a></li>
        <li><a href="../edit_person/1">Person</a></li>
      </ul>
    </li>
    <li><a href="../logout">Logout</a></li>
  </ul>
	</div>
	<br/>
	<br/>
  <div class="container col-sm-4 col-sm-offset-4">
    <form name="personForm" role="form" ng-submit="submitPerson()" ng-show="person.personId">
    	<!-- 
    		{{person}}
      {{personAddresses}}
      {{attributes}}
      {{attributesReplys}}
    	 -->
      <div class="form-group">
        <label for="first_name">First name:</label>
        <input class="form-control" id="first_name" type="text" ng-model="person.firstName" />
      </div>
      <div class="form-group">
        <label for="last_name">Last name:</label>
        <input class="form-control" id="last_name" type="text" ng-model="person.lastName" />
      </div>
      <div class="form-group">
        <label for="identity_code">Identity code:</label>
        <input class="form-control" id="identity_code" type="text" ng-model="person.identityCode" />
      </div>
      <div class="form-group">
        <label for="birth_date">Birth date:</label>
        <input class="form-control" id="birth_date" placeholder="YYYY/MM/DD" type="text" ng-model="person.birthDate" />
      </div>
      <div ng-repeat="personAddress in personAddresses">
        <label>Address {{$index + 1}}</label>
        <div class="form-group">
          <label for="country">Country:</label>
          <input class="form-control" id="country"  type="text" ng-model="personAddress.country" />
        </div>
        <div class="form-group">
          <label for="county">County:</label>
          <input class="form-control" id="county"  type="text" ng-model="personAddress.county" />
        </div>
        <div class="form-group">
          <label for="townVillage">Town/Village:</label>
          <input class="form-control" id="townVillage"  type="text" ng-model="personAddress.townVillage" />
        </div>
        <div class="form-group">
          <label for="address">Address:</label>
          <input class="form-control" id="address"  type="text" ng-model="personAddress.streetAddress" />
        </div>
        <div class="form-group">
          <label for="zipCode">Postal code:</label>
          <input class="form-control" id="zipCode"  type="text" ng-model="personAddress.zipCode" />
        </div>
      </div>
      <div ng-repeat="attribute in attributes">
        <div class="form-group">
          <label >{{attribute.typeName}}</label>
          <input class="form-control" type="text" ng-model="attribute.reply"/>
        </div>
      </div>
      <button class="btn btn-info btn-block" ng-click="addAnotherAddress()">Add another address</button>
      <input class="btn btn-primary btn-block" type="submit" id="submit" value="Submit" />
    </form>
  </div>
</div>
</body>
</html>
